﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SqlTrainer.UserInfoService.Application.Data;
using SqlTrainer.UserInfoService.Persistence.Data;

namespace SqlTrainer.UserInfoService.Persistence.Extensions;

public static class DependencyInjectionExtensions
{
    public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<UserInfoDbContext>(o => o.UseUserInfoDb(configuration));
        
        return services.AddScoped<IUserInfoDbContext, UserInfoDbContext>();
    }
    
    public static void UseUserInfoDb(this DbContextOptionsBuilder options, IConfiguration configuration)
    {
        options.UseNpgsql(configuration.GetConnectionString("UserInfoDb"));
    }
}