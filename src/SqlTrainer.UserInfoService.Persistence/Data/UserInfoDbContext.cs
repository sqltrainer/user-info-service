﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SqlTrainer.UserInfoService.Application.Data;
using SqlTrainer.UserInfoService.Domain.Models;

namespace SqlTrainer.UserInfoService.Persistence.Data;

public class UserInfoDbContext(DbContextOptions<UserInfoDbContext> options) : DbContext(options), IUserInfoDbContext
{
    public DbSet<User> Users { get; set; }
    public DbSet<Role> Roles { get; set; }
    public DbSet<Group> Groups { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        Configure(modelBuilder.Entity<Role>());
        Configure(modelBuilder.Entity<Group>());
        Configure(modelBuilder.Entity<User>());
    }

    private static void Configure(EntityTypeBuilder<Role> entityTypeBuilder)
    {
        entityTypeBuilder.ToTable("roles");
        
        entityTypeBuilder.HasKey(x => x.Id)
            .HasName("roles_id_pk");
        
        entityTypeBuilder.Property(x => x.Id)
            .HasColumnName("id")
            .ValueGeneratedNever()
            .IsRequired();

        entityTypeBuilder.Property(x => x.Name)
            .HasColumnName("name")
            .HasMaxLength(50)
            .IsRequired();

        entityTypeBuilder.HasData(
            new Role(Guid.NewGuid()) { Name = Domain.Constants.Roles.Admin },
            new Role(Guid.NewGuid()) { Name = Domain.Constants.Roles.Teacher },
            new Role(Guid.NewGuid()) { Name = Domain.Constants.Roles.Student },
            new Role(Guid.NewGuid()) { Name = Domain.Constants.Roles.User }
        );
    }

    private static void Configure(EntityTypeBuilder<Group> entityTypeBuilder)
    {
        entityTypeBuilder.ToTable("groups");
        
        entityTypeBuilder.HasKey(x => x.Id)
            .HasName("groups_id_pk");
        
        entityTypeBuilder.Property(x => x.Id)
            .HasColumnName("id")
            .ValueGeneratedOnAdd()
            .IsRequired();

        entityTypeBuilder.Property(x => x.Name)
            .HasColumnName("name")
            .HasMaxLength(50)
            .IsRequired();
    }

    private static void Configure(EntityTypeBuilder<User> entityTypeBuilder)
    {
        entityTypeBuilder.ToTable("users");
        
        entityTypeBuilder.HasKey(x => x.Id)
            .HasName("users_id_pk");

        entityTypeBuilder.HasIndex(x => x.Email).IsUnique();

        entityTypeBuilder.Property(x => x.Id)
            .HasColumnName("id")
            .ValueGeneratedOnAdd()
            .IsRequired();

        entityTypeBuilder.Property(x => x.Name)
            .HasColumnName("name")
            .HasMaxLength(50)
            .IsRequired();

        entityTypeBuilder.Property(x => x.Email)
            .HasColumnName("email")
            .HasMaxLength(100)
            .IsRequired();

        entityTypeBuilder.Property(x => x.HashedPassword)
            .HasColumnName("hash_password")
            .HasMaxLength(100)
            .IsRequired();

        entityTypeBuilder.Property(x => x.RoleId)
            .HasColumnName("role_id")
            .IsRequired();
        
        entityTypeBuilder.Property(x => x.GroupId)
            .HasColumnName("group_id");

        entityTypeBuilder.Property(x => x.FaceImage)
            .HasColumnName("face_image")
            .HasMaxLength(5000);

        entityTypeBuilder.Property(x => x.Rate)
            .HasColumnName("rate");

        entityTypeBuilder.HasOne(x => x.Role)
            .WithMany()
            .HasForeignKey(x => x.RoleId)
            .HasConstraintName("users_roleid_roles_fk");
        
        entityTypeBuilder.HasOne(x => x.Group)
            .WithMany(x => x.Users)
            .HasForeignKey(x => x.GroupId)
            .HasConstraintName("users_groupid_groups_fk");
    }
}