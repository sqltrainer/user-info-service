﻿using Microsoft.AspNetCore.Mvc;
using Results.WebExtensions;
using SqlTrainer.UserInfoService.Application.Users;
using SqlTrainer.UserInfoService.Requests.Users;

namespace SqlTrainer.UserInfoService.Endpoints;

public static class UserEndpoints
{
    public static void MapUserEndpoints(this WebApplication app)
    {
        var group = app.MapGroup("users");
        
        group.MapPost("/", CreateUserAsync);
        group.MapPut("/", UpdateUserAsync);
        group.MapDelete("/{id:required:guid}", DeleteUserAsync);
        group.MapGet("/{id:required:guid}", GetUserAsync);
        group.MapGet("/", GetUsersAsync);
        group.MapGet("/students", GetStudentsAsync);
        group.MapGet("/{email:required}", GetUserByEmailAsync);
    }
    
    private static async Task<IResult> CreateUserAsync(
        [FromServices] IAddUserRequestHandler handler,
        [FromBody] PostUserRequest request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new AddUserRequest(
            request.Name, request.Email, request.Password, request.FaceImage, request.RoleId, request.GroupId);
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> UpdateUserAsync(
        [FromServices] IUpdateUserRequestHandler handler,
        [FromBody] PutUserRequest request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new UpdateUserRequest(
            request.Id, request.Email, request.Name, request.FaceImage, request.RoleId, request.GroupId, request.Rate);
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> DeleteUserAsync(
        [FromServices] IDeleteUserRequestHandler handler,
        [FromRoute] Guid id,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new DeleteUserRequest(id);
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetUserAsync(
        [FromServices] IGetByIdUserRequestHandler handler,
        [FromRoute] Guid id,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new GetByIdUserRequest(id);
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetUsersAsync(
        [FromServices] IGetAllUsersRequestHandler handler,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new GetAllUsersRequest();
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetStudentsAsync(
        [FromServices] IGetAllStudentsRequestHandler handler,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new GetAllStudentsRequest();
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetUserByEmailAsync(
        [FromServices] IGetByEmailUserRequestHandler handler,
        [FromRoute] string email,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new GetByEmailUserRequest(email);
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        return result.ToAspNetResult();
    }
}