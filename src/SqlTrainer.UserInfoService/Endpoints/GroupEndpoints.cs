﻿using Microsoft.AspNetCore.Mvc;
using Results.WebExtensions;
using SqlTrainer.UserInfoService.Application.Groups;
using SqlTrainer.UserInfoService.Requests.Groups;

namespace SqlTrainer.UserInfoService.Endpoints;

public static class GroupEndpoints
{
    public static void MapGroupEndpoints(this WebApplication app)
    {
        var group = app.MapGroup("groups");

        group.MapPost("/", PostGroupAsync);
        group.MapPut("/", PutGroupAsync);
        group.MapDelete("/{id:required:guid}", DeleteGroupAsync);
        group.MapGet("/{id:required:guid}", GetGroupAsync);
        group.MapGet("/", GetGroupsAsync);
    }

    private static async Task<IResult> PostGroupAsync(
        [FromServices] IAddGroupRequestHandler handler,
        [FromBody] PostGroupRequest request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new AddGroupRequest(request.Name);
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        return result.ToAspNetResult();
    }

    private static async Task<IResult> PutGroupAsync(
        [FromServices] IUpdateGroupRequestHandler handler,
        [FromBody] PutGroupRequest request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new UpdateGroupRequest(request.Id, request.Name);
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> DeleteGroupAsync(
        [FromServices] IDeleteGroupRequestHandler handler,
        [FromRoute] Guid id,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new DeleteGroupRequest(id);
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetGroupAsync(
        [FromServices] IGetByIdGroupRequestHandler handler,
        [FromRoute] Guid id,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new GetByIdGroupRequest(id);
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetGroupsAsync(
        [FromServices] IGetAllGroupsRequestHandler handler,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new GetAllGroupsRequest();
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        return result.ToAspNetResult();
    }
}