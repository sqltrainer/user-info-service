﻿using Microsoft.AspNetCore.Mvc;
using Results.WebExtensions;
using SqlTrainer.UserInfoService.Application.Roles;

namespace SqlTrainer.UserInfoService.Endpoints;

public static class RoleEndpoints
{
    public static void MapRoleEndpoints(this WebApplication app)
    {
        var group = app.MapGroup("roles");
        
        group.MapGet("/", GetRolesAsync);
        group.MapGet("/{name:required}", GetRoleByNameAsync);
    }
    
    private static async Task<IResult> GetRolesAsync(
        [FromServices] IGetAllRolesRequestHandler handler,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new GetAllRolesRequest();
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetRoleByNameAsync(
        [FromServices] IGetByNameRoleRequestHandler handler,
        [FromRoute] string name,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new GetByNameRoleRequest(name);
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        return result.ToAspNetResult();
    }
}