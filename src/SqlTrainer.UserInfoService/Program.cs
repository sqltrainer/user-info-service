using SqlTrainer.UserInfoService.Application.Extensions;
using SqlTrainer.UserInfoService.Endpoints;
using SqlTrainer.UserInfoService.Persistence;
using SqlTrainer.UserInfoService.Persistence.Extensions;

var builder = WebApplication.CreateBuilder(args);

builder.Configuration
    .AddUserSecrets<Program>()
    .AddEnvironmentVariables();

builder.Services
    .AddPersistence(builder.Configuration)
    .AddApplication();

const string devCorsPolicy = "DevCorsPolicy";
builder.Services.AddCors(options =>
{
    options.AddPolicy(devCorsPolicy, corsBuilder =>
    {
        corsBuilder
            .SetIsOriginAllowed(origin => new Uri(origin).Host.Equals("localhost"))
            .AllowAnyHeader()
            .AllowAnyMethod();
    });
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

app.MapGroupEndpoints();
app.MapRoleEndpoints();
app.MapUserEndpoints();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseCors(devCorsPolicy);
}

app.Migrate().Run();
