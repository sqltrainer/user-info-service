﻿using System.Text.Json.Serialization;

namespace SqlTrainer.UserInfoService.Requests.Groups;

public sealed class PostGroupRequest
{
    [JsonPropertyName("name")]
    public string Name { get; set; } = null!;
}