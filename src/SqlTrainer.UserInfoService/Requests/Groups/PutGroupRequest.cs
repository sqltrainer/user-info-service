﻿using System.Text.Json.Serialization;

namespace SqlTrainer.UserInfoService.Requests.Groups;

public sealed class PutGroupRequest
{
    [JsonPropertyName("id")]
    public Guid Id { get; set; }
    
    [JsonPropertyName("name")]
    public string Name { get; set; } = null!;
}