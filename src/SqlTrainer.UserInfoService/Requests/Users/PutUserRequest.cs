﻿using System.Text.Json.Serialization;

namespace SqlTrainer.UserInfoService.Requests.Users;

public sealed class PutUserRequest
{
    [JsonPropertyName("id")]
    public Guid Id { get; set; }
    
    [JsonPropertyName("name")]
    public string Name { get; set; } = null!;
    
    [JsonPropertyName("email")]
    public string Email { get; set; } = null!;
    
    [JsonPropertyName("faceImage")]
    public string? FaceImage { get; set; }
    
    [JsonPropertyName("rate")]
    public double Rate { get; set; }
    
    [JsonPropertyName("roleId")]
    public Guid RoleId { get; set; }
    
    [JsonPropertyName("groupId")]
    public Guid? GroupId { get; set; }
}