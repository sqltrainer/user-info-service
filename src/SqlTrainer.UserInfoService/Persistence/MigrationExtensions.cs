﻿using Microsoft.EntityFrameworkCore;
using SqlTrainer.UserInfoService.Persistence.Data;

namespace SqlTrainer.UserInfoService.Persistence;

public static class MigrationExtensions
{
    public static IHost Migrate(this IHost host)
    {
        using var scope = host.Services.CreateScope();
        var services = scope.ServiceProvider;
        
        var loggerFactory = services.GetRequiredService<ILoggerFactory>();
        var logger = loggerFactory.CreateLogger<IHost>();
        
        try
        {
            var dbContext = services.GetRequiredService<UserInfoDbContext>();
            dbContext.Database.Migrate();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "An error occurred while migrating the database.");
        }
        
        return host;
    }
}