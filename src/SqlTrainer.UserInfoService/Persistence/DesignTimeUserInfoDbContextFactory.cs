﻿using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using SqlTrainer.UserInfoService.Persistence.Data;
using SqlTrainer.UserInfoService.Persistence.Extensions;

namespace SqlTrainer.UserInfoService.Persistence;

public sealed class DesignTimeUserInfoDbContextFactory : IDesignTimeDbContextFactory<UserInfoDbContext>
{
    public UserInfoDbContext CreateDbContext(string[] args)
    {
        var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddUserSecrets(Assembly.GetExecutingAssembly())
            .AddEnvironmentVariables()
            .Build();
        
        var builder = new DbContextOptionsBuilder<UserInfoDbContext>();

        builder.UseUserInfoDb(configuration);
        
        Console.WriteLine(configuration.GetConnectionString("UserInfoDb"));

        return new UserInfoDbContext(builder.Options);
    }
}