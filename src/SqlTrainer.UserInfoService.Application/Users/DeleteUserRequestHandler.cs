﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.UserInfoService.Application.Data;

namespace SqlTrainer.UserInfoService.Application.Users;

public sealed record DeleteUserRequest(Guid Id) : IRequest;

public interface IDeleteUserRequestHandler : IRequestHandler<DeleteUserRequest>;

public sealed class DeleteUserRequestHandler(
    IUserInfoDbContext dbContext,
    ILogger<DeleteUserRequestHandler> logger)
    : IDeleteUserRequestHandler
{
    public async Task<IFinalResult> HandleAsync(DeleteUserRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var user = await dbContext.Users.FindAsync([request.Id], cancellationToken);

            if (user is null)
                return Result.Failed("User has not found");

            dbContext.Users.Remove(user);

            await dbContext.SaveChangesAsync(cancellationToken);

            return Result.Success();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "User {Id} has not been removed. Request: {Request}", request.Id, request);
            return UnhandledFailedResult.Create(ex, "User has not been removed");
        }
    }
}