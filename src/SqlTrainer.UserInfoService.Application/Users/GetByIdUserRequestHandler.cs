﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.UserInfoService.Application.Data;
using SqlTrainer.UserInfoService.Domain.Models;

namespace SqlTrainer.UserInfoService.Application.Users;

public sealed record GetByIdUserRequest(Guid Id) : IRequest;

public interface IGetByIdUserRequestHandler : IRequestHandler<GetByIdUserRequest, User>;

public sealed class GetByIdUserRequestHandler(
    IUserInfoDbContext dbContext,
    ILogger<GetByIdUserRequestHandler> logger)
    : IGetByIdUserRequestHandler
{
    public async Task<IFinalResult<User>> HandleAsync(GetByIdUserRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var user = await dbContext.Users
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

            return user is not null
                ? Result.Success(user)
                : Result.Failed<User>("User has not found");
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "User {Id} has not been retrieved. Request: {Request}", request.Id, request);
            return UnhandledFailedResult.Create<User>(ex, "User has not been retrieved");
        }
    }
}