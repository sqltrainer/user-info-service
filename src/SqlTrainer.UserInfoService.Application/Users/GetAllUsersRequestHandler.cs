﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.UserInfoService.Application.Data;
using SqlTrainer.UserInfoService.Domain.Models;

namespace SqlTrainer.UserInfoService.Application.Users;

public sealed record GetAllUsersRequest : IRequest;

public interface IGetAllUsersRequestHandler : IRequestHandler<GetAllUsersRequest, IReadOnlyCollection<User>>;

public sealed class GetAllUsersRequestHandler(
    IUserInfoDbContext dbContext,
    ILogger<GetAllUsersRequestHandler> logger)
    : IGetAllUsersRequestHandler
{
    public async Task<IFinalResult<IReadOnlyCollection<User>>> HandleAsync(GetAllUsersRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var users = await dbContext.Users
                .AsNoTracking()
                .ToListAsync(cancellationToken);

            return Result.Success(users);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Users have not been retrieved. Request: {Request}", request);
            return UnhandledFailedResult.Create<IReadOnlyCollection<User>>(ex, "Users have not been retrieved");
        }
    }
}