﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.UserInfoService.Application.Data;
using SqlTrainer.UserInfoService.Domain.Models;

namespace SqlTrainer.UserInfoService.Application.Users;

public sealed record GetByEmailUserRequest(string Email) : IRequest;

public interface IGetByEmailUserRequestHandler : IRequestHandler<GetByEmailUserRequest, User>;

public sealed class GetByEmailUserRequestHandler(
    IUserInfoDbContext dbContext,
    ILogger<GetByEmailUserRequestHandler> logger)
    : IGetByEmailUserRequestHandler
{
    public async Task<IFinalResult<User>> HandleAsync(GetByEmailUserRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var user = await dbContext.Users
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Email == request.Email, cancellationToken);

            return user is not null
                ? Result.Success(user)
                : Result.Failed<User>("User has not found");
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "User {Email} has not been retrieved. Request: {Request}", request.Email, request);
            return UnhandledFailedResult.Create<User>(ex, "User has not been retrieved");
        }
    }
}