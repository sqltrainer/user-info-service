﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.UserInfoService.Application.Data;

namespace SqlTrainer.UserInfoService.Application.Users;

public sealed record UpdateUserRequest(
    Guid Id,
    string Name,
    string Email,
    string? FaceImagePath,
    Guid RoleId,
    Guid? GroupId,
    double Rate)
    : IRequest;

public interface IUpdateUserRequestHandler : IRequestHandler<UpdateUserRequest>;

public sealed class UpdateUserRequestHandler(
    IUserInfoDbContext dbContext,
    ILogger<UpdateUserRequestHandler> logger)
    : IUpdateUserRequestHandler
{
    public async Task<IFinalResult> HandleAsync(UpdateUserRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var user = await dbContext.Users
                .FindAsync([request.Id], cancellationToken);

            if (user is null)
                return Result.Failed("User has not found");

            user.Email = request.Email;
            user.Name = request.Name;
            user.FaceImage = request.FaceImagePath;
            user.RoleId = request.RoleId;
            user.GroupId = request.GroupId;
            user.Rate = request.Rate;

            await dbContext.SaveChangesAsync(cancellationToken);
            
            return Result.Success();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "User {Id} has not been updated. Request: {Request}", request.Id, request);
            return UnhandledFailedResult.Create(ex, "User has not been updated");
        }
    }
}