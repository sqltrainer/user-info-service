﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.UserInfoService.Application.Data;
using SqlTrainer.UserInfoService.Domain.Models;

namespace SqlTrainer.UserInfoService.Application.Users;

public sealed record AddUserRequest(
    string Name,
    string Email,
    string HashedPassword,
    string? FaceImagePath,
    Guid RoleId,
    Guid? GroupId) 
    : IRequest;

public interface IAddUserRequestHandler : IRequestHandler<AddUserRequest>;

public sealed class AddUserRequestHandler(
    IUserInfoDbContext dbContext,
    ILogger<AddUserRequestHandler> logger)
    : IAddUserRequestHandler
{
    public async Task<IFinalResult> HandleAsync(AddUserRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            User user = new()
            {
                Name = request.Name,
                Email = request.Email,
                FaceImage = request.FaceImagePath,
                HashedPassword = request.HashedPassword,
                RoleId = request.RoleId,
                GroupId = request.GroupId,
                Rate = 0.0
            };

            await dbContext.Users.AddAsync(user, cancellationToken);

            await dbContext.SaveChangesAsync(cancellationToken);
            
            return Result.Success();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "User {Email} has not been added. Request: {Request}", request.Email, request);
            return UnhandledFailedResult.Create(ex, "User has not been added");
        }
    }
}