﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.UserInfoService.Application.Data;
using SqlTrainer.UserInfoService.Domain.Models;

namespace SqlTrainer.UserInfoService.Application.Users;

public sealed record GetAllStudentsRequest : IRequest;

public interface IGetAllStudentsRequestHandler : IRequestHandler<GetAllStudentsRequest, IReadOnlyCollection<User>>;

public sealed class GetAllStudentsRequestHandler(
    IUserInfoDbContext dbContext,
    ILogger<GetAllStudentsRequestHandler> logger)
    : IGetAllStudentsRequestHandler
{
    public async Task<IFinalResult<IReadOnlyCollection<User>>> HandleAsync(GetAllStudentsRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var students = await dbContext.Users
                .Include(u => u.Role)
                .AsNoTracking()
                .Where(u => u.Role!.Name == Domain.Constants.Roles.Student)
                .ToListAsync(cancellationToken);

            return Result.Success(students);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Students have not been retrieved. Request: {Request}", request);
            return UnhandledFailedResult.Create<IReadOnlyCollection<User>>(ex, "Students have not been retrieved");
        }
    }
}