﻿using System.Reflection;
using Application.Mediator.Extensions;
using Microsoft.Extensions.DependencyInjection;

namespace SqlTrainer.UserInfoService.Application.Extensions;

public static class DependencyInjectionExtensions
{
    public static IServiceCollection AddApplication(this IServiceCollection services)
    {
        services.InjectRequestStuff(Assembly.GetExecutingAssembly());
        return services;
    }
}