﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.UserInfoService.Application.Data;
using SqlTrainer.UserInfoService.Domain.Models;

namespace SqlTrainer.UserInfoService.Application.Groups;

public sealed record AddGroupRequest(string Name) : IRequest;

public interface IAddGroupRequestHandler : IRequestHandler<AddGroupRequest>;

public sealed class AddGroupRequestHandler(
    IUserInfoDbContext dbContext,
    ILogger<AddGroupRequestHandler> logger)
    : IAddGroupRequestHandler
{
    public async Task<IFinalResult> HandleAsync(AddGroupRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            Group group = new() { Name = request.Name };

            await dbContext.Groups.AddAsync(group, cancellationToken);
            
            await dbContext.SaveChangesAsync(cancellationToken);
            
            return Result.Success();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Group {Name} has not been added. Request: {Request}", request.Name, request);
            return UnhandledFailedResult.Create(ex, "Group has not been added");
        }
    }
}