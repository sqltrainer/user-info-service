﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.UserInfoService.Application.Data;
using SqlTrainer.UserInfoService.Domain.Models;

namespace SqlTrainer.UserInfoService.Application.Groups;

public sealed record UpdateGroupRequest(Guid Id, string Name) : IRequest;

public interface IUpdateGroupRequestHandler : IRequestHandler<UpdateGroupRequest>;

public sealed class UpdateGroupRequestHandler(
    IUserInfoDbContext dbContext,
    ILogger<UpdateGroupRequestHandler> logger)
    : IUpdateGroupRequestHandler
{
    public async Task<IFinalResult> HandleAsync(UpdateGroupRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            Group group = new(request.Id) { Name = request.Name };

            dbContext.Groups.Update(group);

            await dbContext.SaveChangesAsync(cancellationToken);

            return Result.Success();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Group {Id} has not been updated. Request: {Request}", request.Id, request);
            return UnhandledFailedResult.Create(ex, "Group has not been updated");
        }
    }
}