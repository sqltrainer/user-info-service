﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.UserInfoService.Application.Data;
using SqlTrainer.UserInfoService.Domain.Models;

namespace SqlTrainer.UserInfoService.Application.Groups;

public sealed record GetByIdGroupRequest(Guid Id) : IRequest;

public interface IGetByIdGroupRequestHandler : IRequestHandler<GetByIdGroupRequest, Group>;

public sealed class GetByIdGroupRequestHandler(
    IUserInfoDbContext dbContext,
    ILogger<GetByIdGroupRequestHandler> logger)
    : IGetByIdGroupRequestHandler
{
    public async Task<IFinalResult<Group>> HandleAsync(GetByIdGroupRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var group = await dbContext.Groups
                .AsNoTracking()
                .FirstOrDefaultAsync(g => g.Id == request.Id, cancellationToken);

            return group is not null
                ? Result.Success(group)
                : Result.NotFound<Group>();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Group {Id} has not been retrieved. Request: {Request}", request.Id, request);
            return UnhandledFailedResult.Create<Group>(ex, "Group has not been retrieved");
        }
    }
}