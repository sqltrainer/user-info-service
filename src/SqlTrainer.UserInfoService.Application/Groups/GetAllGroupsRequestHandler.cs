﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.UserInfoService.Application.Data;
using SqlTrainer.UserInfoService.Domain.Models;

namespace SqlTrainer.UserInfoService.Application.Groups;

public sealed record GetAllGroupsRequest : IRequest;

public interface IGetAllGroupsRequestHandler : IRequestHandler<GetAllGroupsRequest, IReadOnlyCollection<Group>>;

public sealed class GetAllGroupsRequestHandler(
    IUserInfoDbContext dbContext,
    ILogger<GetAllGroupsRequestHandler> logger)
    : IGetAllGroupsRequestHandler
{
    public async Task<IFinalResult<IReadOnlyCollection<Group>>> HandleAsync(GetAllGroupsRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var groups = await dbContext.Groups
                .AsNoTracking()
                .ToListAsync(cancellationToken);
            
            return Result.Success(groups);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Groups have not been retrieved");
            return UnhandledFailedResult.Create<IReadOnlyCollection<Group>>(ex, "Groups have not been retrieved");
        }
    }
}