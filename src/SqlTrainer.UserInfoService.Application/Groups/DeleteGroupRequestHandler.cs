﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.UserInfoService.Application.Data;

namespace SqlTrainer.UserInfoService.Application.Groups;

public sealed record DeleteGroupRequest(Guid Id) : IRequest;

public interface IDeleteGroupRequestHandler : IRequestHandler<DeleteGroupRequest>;

public sealed class DeleteGroupRequestHandler(
    IUserInfoDbContext dbContext,
    ILogger<DeleteGroupRequestHandler> logger)
    : IDeleteGroupRequestHandler
{
    public async Task<IFinalResult> HandleAsync(DeleteGroupRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var group = await dbContext.Groups.FindAsync([request.Id], cancellationToken);
            if (group is null)
                return Result.Failed("Group not found");

            dbContext.Groups.Remove(group);
            await dbContext.SaveChangesAsync(cancellationToken);
            
            return Result.Success();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Group {Id} has not been deleted. Request: {Request}", request.Id, request);
            return UnhandledFailedResult.Create(ex, "Group has not been deleted");
        }
    }
}