﻿using Microsoft.EntityFrameworkCore;
using SqlTrainer.UserInfoService.Domain.Models;

namespace SqlTrainer.UserInfoService.Application.Data;

public interface IUserInfoDbContext
{
    DbSet<User> Users { get; }
    DbSet<Role> Roles { get; }
    DbSet<Group> Groups { get; }
    
    Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
}