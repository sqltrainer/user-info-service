﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.UserInfoService.Application.Data;
using SqlTrainer.UserInfoService.Domain.Models;

namespace SqlTrainer.UserInfoService.Application.Roles;

public record GetByNameRoleRequest(string Name) : IRequest;

public interface IGetByNameRoleRequestHandler : IRequestHandler<GetByNameRoleRequest, Role>;

public sealed class GetByNameRoleRequestHandler(
    IUserInfoDbContext dbContext,
    ILogger<GetByNameRoleRequestHandler> logger)
    : IGetByNameRoleRequestHandler
{
    public async Task<IFinalResult<Role>> HandleAsync(GetByNameRoleRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var role = await dbContext.Roles
                .AsNoTracking()
                .FirstOrDefaultAsync(r => r.Name == request.Name, cancellationToken);

            return role is not null
                ? Result.Success(role)
                : Result.NotFound<Role>();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Role {Name} has not been retrieved. Request: {Request}", request.Name, request);
            return UnhandledFailedResult.Create<Role>(ex, "Role has not been retrieved");
        }
    }
}