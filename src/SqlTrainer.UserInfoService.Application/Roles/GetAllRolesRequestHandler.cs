﻿using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.UserInfoService.Application.Data;
using SqlTrainer.UserInfoService.Domain.Models;

namespace SqlTrainer.UserInfoService.Application.Roles;

public sealed record GetAllRolesRequest : IRequest;

public interface IGetAllRolesRequestHandler : IRequestHandler<GetAllRolesRequest, IReadOnlyCollection<Role>>;

public sealed class GetAllRolesRequestHandler(
    IUserInfoDbContext dbContext,
    ILogger<GetAllRolesRequestHandler> logger)
    : IGetAllRolesRequestHandler
{
    public async Task<IFinalResult<IReadOnlyCollection<Role>>> HandleAsync(GetAllRolesRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var roles = await dbContext.Roles
                .AsNoTracking()
                .ToListAsync(cancellationToken);

            return Result.Success(roles);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Roles have not been retrieved. Request: {Request}", request);
            return UnhandledFailedResult.Create<IReadOnlyCollection<Role>>(ex, "Roles have not been retrieved");
        }
    }
}