﻿using Domain.Models;

namespace SqlTrainer.UserInfoService.Domain.Models;

public sealed class Group(Guid id) : IdModel(id), IEquatable<Group>
{
    public Group() : this(default)
    {
    }
    
    public required string Name { get; init; }
    public IReadOnlyCollection<User>? Users { get; private set; }

    public Group WithUsers(IReadOnlyCollection<User> users)
    {
        Users = users;
        return this;
    }

    public override bool Equals(object? obj) => obj is Group other && Equals(other);
    public bool Equals(Group? other) => other is not null && (other.Id == Id || other.Name.Equals(Name));
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(Group? left, Group? right) => left is not null && left.Equals(right);
    public static bool operator !=(Group? left, Group? right) => !(left == right);
    
    public override string ToString() => Name;
}