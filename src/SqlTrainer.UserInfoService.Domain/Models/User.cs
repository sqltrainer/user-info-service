﻿using Domain.Models;

namespace SqlTrainer.UserInfoService.Domain.Models;

public sealed class User(Guid id) : IdModel(id), IEquatable<User>
{
    public User() : this(default)
    {
    }
    
    public required string Name { get; set; }
    public required string Email { get; set; }
    public string? HashedPassword { get; init; }
    public required Guid RoleId { get; set; }
    public Role? Role { get; private set; }
    public Guid? GroupId { get; set; }
    public Group? Group { get; private set; }
    public string? FaceImage { get; set; }
    public double Rate { get; set; }

    public User WithRole(Role role)
    {
        Role = role;
        return this;
    }
    
    public User WithGroup(Group group)
    {
        Group = group;
        return this;
    }

    public override bool Equals(object? obj) => obj is User user && Equals(user);
    public bool Equals(User? other) => other is not null && (other.Id == Id || other.Email.Equals(Email));
    public override int GetHashCode() => base.GetHashCode();

    public static bool operator ==(User? left, User? right) => left is not null && left.Equals(right);
    public static bool operator !=(User? left, User? right) => !(left == right);

    public override string ToString() => $"{Email} ({Name})";
}