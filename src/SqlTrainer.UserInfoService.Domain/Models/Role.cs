﻿using Domain.Models;

namespace SqlTrainer.UserInfoService.Domain.Models;

public sealed class Role(Guid id) : IdModel(id), IEquatable<Role>
{
    public Role() : this(default)
    {
    }
    
    public required string Name { get; init; }

    public override bool Equals(object? obj) => obj is Role other && Equals(other);
    public bool Equals(Role? other) => other is not null && (other.Id == Id || other.Name.Equals(Name));
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(Role? left, Role? right) => left is not null && left.Equals(right);
    public static bool operator !=(Role? left, Role? right) => !(left == right);
    
    public override string ToString() => Name;
}