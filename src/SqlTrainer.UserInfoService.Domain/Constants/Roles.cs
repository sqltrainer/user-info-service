﻿namespace SqlTrainer.UserInfoService.Domain.Constants;

public static class Roles
{
    public const string Admin = "Administrator";
    public const string Student = "Student";
    public const string Teacher = "Teacher";
    public const string User = "Simple User";
}